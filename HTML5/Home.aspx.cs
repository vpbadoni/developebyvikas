﻿using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.Util;
using AWSSignatureV4_S3_Upload_Verification;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Text;

namespace HTML5
{
    public partial class Home : System.Web.UI.Page
    {
        List<string> filename = new List<string>();
        string accessKey = System.Configuration.ConfigurationManager.AppSettings["accessKey"];
        string secretKey = System.Configuration.ConfigurationManager.AppSettings["secretKey"];
        protected void Page_Load(object sender, EventArgs e)
        {
            using (IAmazonS3 s3Client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1))
            {
                PutObjectResponse response1 = null;
                try
                {
                    PutObjectRequest request = new PutObjectRequest();
                    request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
                    response1 = s3Client.PutObject(request);
                }
                catch (Exception ex)
                {

                }

            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            string[] uploadedfiles = hdn.Value.Split('~');
            string userName = txtusername.Text;
            string projectName = txtprojectname.Text;

            Boolean fileStatus = CheckS3ProjectFiles(userName, projectName);

            HiddenField1.Value = "";
            string path = @"C:\VideoEditor\Projects\" + txtusername.Text + @"\" + txtprojectname.Text + @"\" + "TrackListConfig.csv";

            if (fileStatus)
            {
                //Response.Redirect("Dashboard.aspx?Usr=" + userName + "&pro=" + projectName);
                id_upload.Style.Add("visibility", "visible");
                //id_upload.Style.Add("visibility", "visible");
                btnsubmit.Style.Add("visibility", "hidden");
            }
            else
            {

                Page.ClientScript.RegisterStartupScript(this.GetType(), "msgbxConfirm", "CreateProjectFolder();", true);
                System.IO.Directory.CreateDirectory(@"C:\VideoEditor\Projects\" + userName + "\\" + projectName);


                using (var stream = File.CreateText(@"C:\VideoEditor\Projects\" + userName + "\\" + projectName + "\\" + "TrackListConfig.csv"))
                {
                    string csvRow = string.Format("{0},{1},{2},{3}", "MediaFileIndex", "FileName", "Url", "Config");

                    stream.WriteLine(csvRow);
                }



            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            buildFormAndPolicy();
        }

        protected void buildFormAndPolicy()
        {
            List<PostS3AWSPolicyCondition> conditions = new List<PostS3AWSPolicyCondition>();

            // Remove btnUpload since it was corrupting the policy and thus authentication
            box.Controls.Clear();

            // Add existing and auto generated form input elements to the list
            foreach (string key in Request.Form.Keys)
            {
                if (key.Equals("id_upload") || key.Equals("btnUpload"))
                    continue;
                PostS3AWSPolicyCondition condition = new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH, key, "");
                conditions.Add(condition);
            }

            List<PostS3AWSPolicyCondition> addedConditions = PostS3ObjectAWSSignatureV4.GetAWSCredentialV4(conditions, txtusername.Text, txtprojectname.Text);

            homepage.Action = PostS3ObjectAWSSignatureV4.GetUrl().ToString();

            // Loop through all the newly added conditions and add them to the Form
            foreach (PostS3AWSPolicyCondition condition in addedConditions)
            {
                if (condition.includeInHtmlForm)
                {
                    HtmlInputText input = new HtmlInputText();
                    input.ID = condition.name;
                    if (input.ID.Equals("key"))
                        input.Value = condition.value + "${filename}";
                    else
                        input.Value = condition.value;
                    homepage.Controls.Add(input);
                    homepage.Controls.Add(new LiteralControl("\n"));
                }
            }

            // Add file input form field last since AWS ignores all fields in the request after the file field
            HtmlInputFile fileInput = new HtmlInputFile();
            fileInput.ID = "file";
            fileInput.Name = "submit";
            fileInput.Attributes.Add("multiple", "multiple");
            //fileInput.Attributes.Add("OnClick", "btnUpload_Click");
            homepage.Controls.Add(fileInput);
            homepage.Controls.Add(new LiteralControl("\n"));
            btnUpload.Style.Add("visibility", "visible");
            //btnUpload.Style.Add("onClick", "redirecttodashboard");
            btnsubmit.Style.Add("visibility", "hidden");
        }


        [WebMethod]
        public static Boolean CheckS3ProjectFiles(string usrName, string prjName)
        {
            Boolean fileStatus = false;
            using (IAmazonS3 s3Client = new AmazonS3Client(System.Configuration.ConfigurationManager.AppSettings["accessKey"], System.Configuration.ConfigurationManager.AppSettings["secretKey"], Amazon.RegionEndpoint.USEast1))
            {
                try
                {
                    // S3DirectoryInfo s3DirectoryInfo = new Amazon.S3.IO.S3DirectoryInfo(s3Client, rootBucket + "/" + usrName + "/" + prjName);

                    AmazonS3Client client = new AmazonS3Client(System.Configuration.ConfigurationManager.AppSettings["accessKey"], System.Configuration.ConfigurationManager.AppSettings["secretKey"], Amazon.RegionEndpoint.USEast1);

                    ListObjectsRequest request = new ListObjectsRequest();
                    request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
                    request.Prefix = usrName + "/" + prjName + "/";
                    ListObjectsResponse response = client.ListObjects(request);

                    var count = response.S3Objects.Count;
                    if (count > 0)
                    {
                        fileStatus = true;
                    }
                    else
                    {
                        fileStatus = false;
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    fileStatus = false;
                }

            }

            return fileStatus;
        }


        [WebMethod]

        public static string CreateBucket(string user, string project)
        {
            PutObjectResponse response1 = null;
            var s3Client = new AmazonS3Client(System.Configuration.ConfigurationManager.AppSettings["accessKey"], System.Configuration.ConfigurationManager.AppSettings["secretKey"], Amazon.RegionEndpoint.USEast1);
            string bucketName = user + "." + project;
            try
            {
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
                request.ContentBody = "MediaFileIndex,FileName,Url,Config";
                request.Key = user + "/" + project + "/" + "TrackListConfig.csv";
                response1 = s3Client.PutObject(request);
            }
            catch (Exception ex)
            {
                return ex.InnerException.ToString();
            }


            return response1.HttpStatusCode.ToString();

        }

        [WebMethod]

        public static string btnSubmit_Click(string username, string projectname, string uploadedfiles)
        {
            string accessKey = System.Configuration.ConfigurationManager.AppSettings["accessKey"];
            string secretKey = System.Configuration.ConfigurationManager.AppSettings["secretKey"];

            List<UploadedFiles> _files = new List<UploadedFiles>();
            _files = JsonConvert.DeserializeObject<List<UploadedFiles>>(uploadedfiles);

            string path = @"C:\VideoEditor\Projects\" + username + @"\" + projectname + @"\" + "TrackListConfig.csv";
            List<string> existingFiles = new List<string>();
            if (File.Exists(path))
            {
                string result = "";
                try
                {
                    // Open the file to read from.
                    string[] readText = File.ReadAllLines(path);
                    int i = 0;
                    foreach (string s in readText)
                    {
                        if (i == 0)
                        {
                            i = 1;
                            continue;
                        }
                        string[] items = s.Split(',');
                        existingFiles.Add(items[1]);

                    }
                }
                catch (Exception ex)
                {

                }
            }

            if (_files.Count > 0)
            {
                foreach (var file in _files)
                {

                    if (!existingFiles.Contains(file.original_name))
                    {

                        using (IAmazonS3 s3Client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1))
                        {
                            try
                            {
                                AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1);

                                ////Check for All the files existing in that folder
                                ////getting an object from bucket
                                //ListObjectsRequest request = new ListObjectsRequest();
                                //request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
                                //request.Prefix = username + "/" + projectname + "/";
                                //ListObjectsResponse response = client.ListObjects(request);
                                //var count = response.S3Objects.Count;
                                //if (count > 0)
                                //{
                                //    //Compare the uploaded files with Response of the above
                                //    //We will compare and ignore in the s3 response which are already there
                                //    List<string> _notCommon = response.S3Objects.Where(z => !z.Key.Contains("TrackListConfig.csv")).Select(x => x.Key).Except(_files.Select(y => y.s3_name)).ToList();

                                //    for (int k = 0; k < _notCommon.Count(); k++)
                                //    {
                                //        string _onlyName = _notCommon[k].Replace(username + "/" + projectname + "/", "");

                                //        //Add all those files (UploadedFiles) which are not in s3 response
                                //        _files.Add(new UploadedFiles()
                                //        {
                                //            original_name = _onlyName,
                                //            s3_name = "/" + username + "/" + projectname + "/" + _onlyName,
                                //            size = "0",
                                //            url = "https://s3.amazonaws.com/" + request.BucketName + "/" + username + "/" + projectname + "/" + _onlyName
                                //        });
                                //    }

                                string subDirectoryInBucket = username + "/" + projectname;
                                string projectBucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
                                if (subDirectoryInBucket != null && subDirectoryInBucket != "")
                                {   // subdirectory and bucket name
                                    projectBucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"] + @"/" + subDirectoryInBucket;
                                }

                                // Download the music file because we need it to determine tag counts
                                if (file.original_name.EndsWith(".mp3") || file.original_name.EndsWith(".wav"))
                                {
                                    String musicFilePath = @"C:\VideoEditor\Projects\" + username + @"\" + projectname + @"\" + file.original_name;
                                    if (!File.Exists(musicFilePath))
                                    {
                                        // Download if it does not already exist
                                        TransferUtility utility = new TransferUtility(client);
                                        utility.Download(musicFilePath, projectBucketName, file.original_name);
                                    }
                                }
                                //}

                                int pageIndex = 0;

                                String last = File.ReadLines(path).Last();

                                string index = last.Split(',')[0];
                                if (!index.Equals("MediaFileIndex"))
                                {
                                    pageIndex = Convert.ToInt32(index);
                                }

                                File.AppendAllText((path), string.Format("{0},{1},{2},{3}", pageIndex + 1, file.original_name, "https://s3.amazonaws.com/" + projectBucketName + "/" + file.original_name, "\r\n"));
                            }
                            catch (AmazonS3Exception ex)
                            {

                            }

                        }
                    }

                }

            }

            return "true";
        }

        public class UploadedFiles
        {
            public string original_name { get; set; }
            public string s3_name { get; set; }
            public string url { get; set; }
            public string size { get; set; }
        }

        public class RootObject
        {
            public List<UploadedFiles> Files { get; set; }
        }
    }
}

