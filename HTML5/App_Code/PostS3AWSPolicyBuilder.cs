﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using AWSSignatureV4_S3_Upload_Verification.Signers;

/// <summary>
/// Summary description for PostPolicyBuilder
/// https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html
/// Example Policy:
/// 
//{ "expiration": "2018-25-10T12:25:48.956Z",
//  "conditions": [
//    {"bucket": "bucket_name"},
//    ["starts-with", "$key", "user/user1/"],
//    {"acl": "public-read"},
//    {"success_action_status": "201"},
//    ["starts-with", "$Content-Type", "image/"],
//    {"x-amz-meta-uuid": "14365123651274"},
//    {"x-amz-server-side-encryption": "AES256"},
//    ["starts-with", "$x-amz-meta-tag", ""],

//    {"x-amz-credential": "AKIAIOSFODNN7EXAMPLE/20180209/us-east-1/s3/aws4_request"},
//    {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
//    {"x-amz-date": "20180209T002548Z" }
//  ]
//}
/// </summary>
public class PostS3AWSPolicyBuilder
{
    static readonly string awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
    static readonly string awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
    static readonly string bucketName = ConfigurationManager.AppSettings["rootBucket"];
    static readonly string region = ConfigurationManager.AppSettings["region"];

    private string policy;

    public PostS3AWSPolicyBuilder(List<PostS3AWSPolicyCondition> conditions, DateTime dateTimeStamp)
    {
        this.policy = buildPolicy(conditions, dateTimeStamp);
    }

    private string buildPolicy(List<PostS3AWSPolicyCondition> conditions, DateTime dateTimeStamp)
    {
        var policyBuilder = new StringBuilder();

        policyBuilder.AppendFormat("{{ \"expiration\": \"{0}\",\n", dateTimeStamp.AddDays(1).ToString(AWS4SignerBase.ISO8601GMTFormat));
        policyBuilder.Append(  "\"conditions\" : [\n");

        foreach (PostS3AWSPolicyCondition condition in conditions)
        {
            if (condition.matchType.Equals(PostS3AWSPolicyCondition.MATCHTYPE_EXACT))
            {
                policyBuilder.AppendFormat(    "{{ \"{0}\": \"{1}\"}},\n", condition.name, condition.value);
            }
            else if (condition.matchType.Equals(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH) || condition.matchType.Equals(PostS3AWSPolicyCondition.MATCHTYPE_RANGE))
            {
                policyBuilder.AppendFormat(    "[ \"{0}\", \"${1}\", \"{2}\"],\n", condition.matchType, condition.name, condition.value);
            }
            else
                throw new Exception("Unexpected PostPolicyCondition match type: " + condition.ToString());

        }

        policyBuilder.Append(  "]\n}");

        return policyBuilder.ToString();

    }

    public string getPolicy()
    {
        return policy;
    }
}