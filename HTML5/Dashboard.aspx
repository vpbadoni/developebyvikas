﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="HTML5.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title></title>
    <style>
        .fragment {
            font-size: 12px;
            font-family: tahoma;
            height: 100px;
            border: 1px solid #ccc;
            color: #555;
            display: inline-block;
            box-sizing: border-box;
            text-decoration: none;
            /* width: 100px;*/
            /*float: left;*/
        }

            .fragment:hover {
                box-shadow: 2px 2px 5px rgba(0,0,0,.2);
            }

        .list-group-item {
            height: 140px;
        }

        .list-group-item-tag {
            height: 140px;
        }

        .fragment img {
            float: left;
            margin-right: 10px;
        }


        .fragment h3 {
            padding: 0;
            margin: 0;
            color: #369;
        }

        .fragment h4 {
            padding: 0;
            margin: 0;
            color: #000;
        }

        #close {
            float: right;
            display: inline-block;
            padding: 2px 5px;
            background: #ccc;
            cursor: pointer;
            margin-left: -50px;
        }

        article {
            background: white;
            padding: 10%;
        }

        video {
            width: 100% !important;
            height: auto !important;
        }
        /*#playlist {
            display: table;
            list-style-type: none;
        }

            #playlist li {
                cursor: pointer;
                padding: 8px;
            }
            

                #playlist li:hover {
                    color: blue;
                }*/


        #videoarea {
            float: left;
            width: 520px;
            height: 240px;
            margin: 10px;
            border: 1px solid silver;
        }

        #img li {
            float: left;
            list-style-type: none;
            margin: 0px 5px 0px 0px;
        }

        #img ul {
            list-style-type: none;
        }

        .cat {
            border: 2px solid white;
        }

        .background_selected {
            border: 2px solid red;
        }

        #DVWaveForm {
            width: 80%;
            height: 120px;
            overflow-y: scroll;
            margin: 0 auto;
        }

        #DVMP3File {
            width:100%;
            margin: 0 auto;
            
        }

            #DVMP3File div {
                width: 100%;
                display: block;
                padding-left: 0;
                padding-right: 0;
            }

                #DVMP3File div a {
                    cursor: pointer;
                    list-style: none;
                    width: 100%;
                    height:120px;
                    float: left;
                    height: 100px;
                    border:3px solid;
                    border-color:rgb(103, 247, 233) rgb(103, 247, 233) rgb(103, 247, 233) rgb(221, 221, 221);
                    text-align: center;
                    padding-left: 0;
                    padding-right: 0;
                }


        #waveform {
            width: 100%;
            margin: 0 auto;
            background: rgba(0,0,0,0.8);
            float: right;
        }

        #waveform-timeline {
            width: 100%;
            height:30px;
            margin: 0 auto;
            float: right;
            padding-top: 5px;
        }

        #DContainer {
            width: 100%;
            margin: 0 auto;
        }
    </style>
    <script src="jquery-3.2.1.min.js"></script>
    <script src="jquery.repeater.js"></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    

    <script src="contextMenu.js"></script>
    <link href="contextMenu.css" rel="stylesheet" />
    <script src="OldJavascript.js"></script>

    <script src="js/wavesurfer.min.js"></script>

    <script src="js/ResionWavesurfer.js"></script>
    <script src="js/TimelineWaveSurfer.js"></script>
    <script src="js/WaveformJavascript.js"></script>

    <script>
        function LockScrollMe()
        {
            var Pdiv = document.getElementById('main');
            document.body.style.overflow = 'hidden';
            Pdiv.style.overflow = 'hidden';
        }

        function UnLockScrollMe() {
            var Pdiv = document.getElementById('main');
            document.body.style.overflow = 'auto';
            Pdiv.style.overflow = 'auto';
        }

        function PlayPauseMP3()
        {
            event.preventDefault();
            wavesurfer.playPause();
            return false;
        }
    </script>


</head>

<body>

    <form id="form1" runat="server" class="outer-repeater">
        <div>
            <%--<div style="height: 50px; overflow-x: scroll;">
            <div data-repeater-list="outer-group" class="outer">
                <div data-repeater-item class="outer">
                    <input type="text" name="text-input" value="A" class="outer" />
                    <input data-repeater-delete type="button" value="Delete" class="outer" />

                </div>
            </div>
            <input data-repeater-create type="button" value="Add" class="outer" />
        </div>--%>
            <div class="page-header" style="margin: 0px 0px 0px 0px; padding-bottom: 0px; white-space: nowrap;">
                <h1>Clip Selection & Configuration</h1>
                <asp:HiddenField ID="hdnuser" runat="server" />
                <asp:HiddenField ID="hdnproject" runat="server" />
                <asp:HiddenField ID="hdn" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
            </div>

            <article>
                <video id="videoarea" width="640" height="480" class="vConMenu" controls="controls" poster="" src=""></video>
                <video id="videoarea1" width="640" height="480" style="display: none;" class="vConMenu" controls="controls" poster="" src=""></video>
            </article>
            <div style="width: 90%; clear: both; font-weight: 300; white-space: nowrap; text-align: center; margin: auto;">
                <div style="float: left; font-weight: 400; width: 20%; margin-top: 50px; white-space: nowrap;">
                    <label>Selection Tag Count:</label>
                    <label id="selCount">-1</label>
                </div>
                <div style="float: left; font-weight: 400; width: 20%; margin-top: 50px; white-space: nowrap;">
                    <label>Min:</label>
                    <label id="minCount">-1</label>
                </div>
                <div style="float: left; font-weight: 400; width: 20%; margin-top: 50px; white-space: nowrap;">
                    <label>Needed:</label>
                    <label id="needCount" style="color: lawngreen">-1</label>
                </div>
                <div style="float: left; font-weight: 400; width: 20%; margin-top: 50px; white-space: nowrap;">
                    <label>Available:</label>
                    <label id="avaiCount" style="color: red">-1</label>
                </div>
                <div style="float: left; font-weight: 400; width: 20%; margin-top: 50px; white-space: nowrap;">
                    <label>Max:</label>
                    <label id="maxCount">-1</label>
                </div>
            </div>

            <div style="clear: both;"></div>
            <div style="width: 90%; clear: both; margin-top: 30px; white-space: nowrap; text-align: center; margin-left: auto; margin-right: auto;">
                <button id="buttonLeft" type="button" onclick="left()" class="btn btn-primary btn-sm" title="Previous Tag"><< TAG</button>
                <span style="margin-left: 30px;"></span>
                <button id="button" type="button" onclick="snap()" class="btn btn-primary btn-sm" title="Add tag in timeline">Add TAG</button>
                <span style="margin-left: 30px;"></span>
                <button id="buttonRight" type="button" onclick="right()" class="btn btn-primary btn-sm" title="Next Tag">TAG >></button>
            </div>
            <div>
                <br />
                <span class="label">Timeline</span>
            </div>
            <div style="clear: both;"></div>

            <%--<div style="float: left; margin-left: 50px; margin-top: 11px; height: 240px; overflow-x: hidden; overflow-y: no-display">--%>
            <div id="main" style="width: 90%; height: 400px; clear: both; white-space: nowrap; text-align: center; margin-left: auto; margin-right: auto; overflow-y: scroll">
                <div id="divtaglist" style="width: 100%; padding-top:10px;">
                    <div class="row" style="margin:0px;">
                        <div class="col-sm-2">
                            <div id="DVMP3File" onclick="return PlayPauseMP3();">
                                <div id="playlistbox" class="col-md-12"></div>
                            </div>
                            <div class="col-sm-12" style="margin:0 auto;" id="btnDiv">
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div id="waveform" onmouseover="LockScrollMe();" onmouseout="UnLockScrollMe();"></div>
                            <div id="waveform-timeline"></div>
                        </div>
                    </div>
                    <div class="row" style="margin: 0px; padding-top:10px;">
                        <div class="col-sm-2" style="display: inline-block; vertical-align: top;">
                            <ul id="playlist" class="list-group" style="" runat="server">
                            </ul>
                        </div>
                        <div class="col-sm-10" style="display: inline-block; vertical-align: top; margin-left: -5px; text-align: left;">

                            <div style="clear: both;"></div>
                            <ul id="tagList" class="list-group" runat="server">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: left; margin-left: 200px;">
                <div style="margin-top: 50px;">
                    <h3><span class="label label-default" title="Configuration Settings">Configuration Settings </span></h3>
                </div>
                <div>
                    <table style="border-collapse: separate; border-spacing: 5px;">
                        <tr>
                            <td title="Decimator Help Text">Decimator
                            </td>
                            <td>

                                <input type="number" title="decimator field help text" id="txtdecimator" value="2" runat="server" name="txtdecimator" min="0" max="16" onchange="this.value = minmax(this.value, 0, 16)" />

                            </td>
                            <td title="Payback Rate Help Text">Playback Rate
                            </td>
                            <td>

                                <input type="number" title="playback rate field help text" id="txtplayback" runat="server" value="0.5" name="txtplayback" min="0.5" max="3" onchange="this.value = minmax(this.value, 0.5, 3)" />

                            </td>
                        </tr>
                        <tr>
                            <td title="Selection Method Help Text">
                                <h4>Selection Method </h4>
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="optradioselmethod" Width="50%" RepeatDirection="Horizontal" runat="server" ToolTip="selection method field help text">
                                    <asp:ListItem Value="Random">Random</asp:ListItem>
                                    <asp:ListItem Value="User Input" Selected="True">User Input</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td title="Clip Order Help Text">
                                <h4>Clip Order </h4>
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="optradio" runat="server" ToolTip="clip order field help text">
                                    <asp:ListItem Value="Chronological">Chronological</asp:ListItem>
                                    <asp:ListItem Value="Random">Random</asp:ListItem>
                                    <asp:ListItem Value="Random(Maintain First & Last)"> Random(Maintain First & Last)</asp:ListItem>
                                </asp:RadioButtonList>
                                <%-- <label class="radio-inline"><input type="radio" name="optradio" checked="checked" />Chronological</label>--%>
                            </td>
                            <%--<td>                                    
                                    <label class="radio-inline"><input type="radio" name="optradio" />Random</label>
                                </td>
                                <td>                                    
                                    <label class="radio-inline"><input type="radio" name="optradio" />Random(Maintain First & Last)</label>
                                </td>   --%>
                        </tr>
                        <tr>
                            <td title="Transition Time Help Text">
                                <h4>Transition Time</h4>
                            </td>
                            <td>
                                <input type="text" title="transition time field help text" id="txtTrantime" runat="server" value="350ms" class="btn btn-default btn-sm" />
                            </td>
                        </tr>
                        <tr>
                            <td title="Save Button Help Text">

                                <button type="button" class="btn btn-primary" onclick="confirm('Save project and continue later?');if(true){savedata();}" title="Save">Save</button>
                            </td>
                            <td title="Submit & Render Button Help Text">

                                <button type="button" class="btn btn-primary" onclick="confirm('Submit & Render the project?');if(true){submitRender();}" title="Save & Render">Submit & Render</button>
                            </td>
                            <td>
                                <asp:Label ID="lblstatus" runat="server" ForeColor="Green" Font-Bold="true" Text="Not Started"></asp:Label>&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>

                </div>
            </div>

        </div>


        <button type="button" class="btn btn-primary" onclick="confirm('Do you want to download final edited video ?');if(true){downloadFile();}" title="Download final edit">Download Final Edit</button>
    </form>
    <script>
        function ConfirmAndExecute() {
            if (confirm("Matched files :- " + document.getElementById("HiddenField1").value + "!! \n \n Do want to continue ?")) {

            }
            else {
                document.getElementById("selectedFiles").innerHTML = "";
                document.getElementById("hdn").value = "";
                document.getElementById("id_upload").value = "";
            }

        }


        function handleFileSelect() {
            'use strict'
            var URL = window.URL || window.webkitURL

            var elem = document.getElementById("id_upload");
            var names = [];
            var fileURLs = [];
            for (var i = 0; i < elem.files.length; ++i) {
                names.push(elem.files[i].name);
                fileURLs.push(URL.createObjectURL(elem.files[i]));
                var selDiv = document.getElementById("selectedFiles");
                var hdn = document.getElementById("hdn");
                if (!selDiv.innerHTML.includes(elem.files[i].name)) {
                    selDiv.innerHTML += elem.files[i].name + "<br/>"
                    hdn.value += "~" + elem.files[i].name + "@" + URL.createObjectURL(elem.files[i]);
                }
            }
        }

        function submitRender() {
            var liList = document.getElementById("playlist").getElementsByTagName("li");
            var FileWithTagData = [];
            var json;
            for (var i = 0; i < liList.length; i++) {
                json = liList[i].textContent + "@@@"
                var divid = liList[i].id.split("_")[0] + "_tag";
                var list = document.getElementById(divid);
                json += liList[i].attributes[3].textContent + "@@@";
                var items = list.childNodes;
                var itemsArr = [];
                for (var a in items) {
                    if (items[a].nodeType == 1) { // get rid of the whitespace text nodes
                        itemsArr.push(items[a].childNodes["0"].childNodes[2].innerHTML);
                    }
                }
                json += itemsArr.toString();

                FileWithTagData.push(json);
                json = "";
            }
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/submitRender",
                data: "{'json':'" + JSON.stringify(FileWithTagData) + "' ,'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    document.getElementById("lblstatus").innerHTML = response.d;
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }


        function downloadFile() {
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/downloadFile",
                data: "{'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                },
                failure: function (response) {
                    // alert(response.d);
                }
            });
        }

        function savedata() {
            var liList = document.getElementById("playlist").getElementsByTagName("li");
            var FileWithTagData = [];
            var json;
            var ConfigJson;
            for (var i = 0; i < liList.length; i++) {
                json = liList[i].id.split("_")[0] + "@@@";
                json += liList[i].textContent + "@@@"
                var divid = liList[i].id.split("_")[0] + "_tag";
                var list = document.getElementById(divid);
                json += liList[i].attributes[3].textContent + "@@@";
                json += ($(liList[i]).attr('reverse') == '1' ? 1 : 0) + '_' + ($(liList[i]).attr('flip') == '1' ? 1 : 0) + "@@@";
                var items = list.childNodes;
                var itemsArr = [];
                for (var a in items) {
                    if (items[a].nodeType == 1) { // get rid of the whitespace text nodes
                        var val = liList[i].id.split("_")[0] + '_' + ($(items[a]).attr('time') ? $(items[a]).attr('time') : 0) + '_';

                        var configVal = '';
                        var k = 0;
                        for (k = 0; k < configs.length; k++) {
                            if ($(items[a]).attr(configs[k]) == '1') {
                                configVal += '*1';
                            } else {
                                configVal += '*0';
                            }
                        }
                        val += configVal.substr(1);

                        itemsArr.push(val);
                    }
                }
                json += itemsArr.toString();

                FileWithTagData.push(json);
                json = "";

            }
            var ClipOrder = $('input[name=optradio]:checked').val();
            var tranTime = $('#txtTrantime').val();
            var decimator = $('#txtdecimator').val();
            var playback = $('#txtplayback').val();
            var selectionMethod = $('#optradioselmethod input:checked').val();

            ConfigJson = ClipOrder + "@@@" + tranTime + "@@@" + decimator + "@@@" + playback + "@@@" + selectionMethod;

            FileWithTagData.push(ConfigJson);
            ConfigJson = "";
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/saveData",
                data: "{'json':'" + JSON.stringify(FileWithTagData) + "' ,'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.ajax({
                        type: "POST",
                        url: "Dashboard.aspx/SaveDataTos3",
                        contentType: "application/json; charset=utf-8",
                        data: "{'json':'" + JSON.stringify(FileWithTagData) + "' ,'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "'}",
                        success: function (response) {
                            // alert(response.d);
                        },
                        failure: function (response) {
                            // alert(response.d);
                        }
                    });
                },
                failure: function (response) {
                    // alert(response.d);
                }
            });


        }


        document.onkeydown = function (e) {
            switch (e.keyCode) {
                case 37:
                    left();
                    break;
                case 39:
                    right();
                    break;
                case 68:
                case 74:
                    moveLeft();
                    break;
                case 70:
                case 75:
                    moveRight();
                    break;
            }
        };

        // Get handles on the video and canvas elements
        var video = document.querySelector('video');
        var canvas = document.querySelector('canvas');
        var cVideoTotalTag = 0;
        var cVideoCurrent = 0;
        var videoStartTime = 0;
        var currentShot = null;


        // Define some vars required later
        var w, h, ratio;

        // Add a listener to wait for the 'loadedmetadata' state so the video's dimensions can be read
        video.addEventListener('loadedmetadata', function () {
            // Calculate the ratio of the video's width to height
            ratio = video.videoWidth / video.videoHeight;
            // Define the required width as 100 pixels smaller than the actual video's width
            w = video.videoWidth - 100;
            // Calculate the height based on the video's width and the ratio
            h = parseInt(w / ratio, 10);

            this.currentTime = videoStartTime;

            // Set the canvas width and height to the values just calculated



        }, false);

        video.addEventListener("click", function (event) {
            if (video.paused == true) {
                video.play();
            }
            else {
                video.pause();
            }
        });

        // Takes a snapshot of the video
        function snap() {
            console.log('Taking snapshot of video');
            document.getElementById("lblstatus").innerHTML = "In Progress";
            var a = $("#videoarea").attr("src");
            if (a.slice(-3) != "mp3" && a.slice(-3) != "MP3") {

                var ul = document.getElementById("tagList");
                var vid = document.querySelector('video');

                var li = document.getElementById($("#button").val() + "_tag");
                li.style.height = '140px';
                li.style.overflowX = 'auto';
                li.style.overflowY = 'hidden';
                // li.style.width = '4000px';

                var m = parseInt((vid.currentTime / 60) % 60);
                //var s = parseInt(vid.currentTime % 60);
                var s = (parseFloat(vid.currentTime % 60).toFixed(4));
                if (s < 10) {
                    s = '0' + s;
                }




                //check if tag time already exist or not 

                var items = li.childNodes;
                var itemsArr = [];
                for (var i in items) {
                    if (items[i].nodeType == 1) { // get rid of the whitespace text nodes
                        itemsArr.push(items[i]);
                    }
                }


                for (var i in itemsArr) {
                    var ms = m + ':' + s.toString().split('.')[0];

                    if (itemsArr[i].childNodes["0"].childNodes[2].innerHTML == ms) {
                        return false;
                    }
                }

                $('#tagList a').css('border-color', '#ccc');
                $('#tagList a').css('border-width', '1px');
                currentcount++;

                document.getElementById('needCount').textContent = document.getElementById('needCount').textContent - 1;
                document.getElementById('avaiCount').textContent = document.getElementById('avaiCount').textContent - 1;
                var atg = document.createElement("a");
                atg.className = "fragment";
                atg.style.borderColor = 'red';
                atg.style.borderWidth = '2px';

                var myDiv = document.createElement("div");
                var sp = document.createElement('span');
                sp.innerHTML = 'X';
                sp.id = 'close';
                sp.onclick = spanClick;

                myDiv.style.height = '90px';
                myDiv.style.cssFloat = "left";
                myDiv.style.padding = '10px';
                myDiv.appendChild(sp);


                ratio = video.videoWidth / video.videoHeight;

                height = 70;
                width = parseInt(height * ratio, 10);

                var canva = document.createElement("canvas");
                canva.id = li.id + "_" + li.childNodes.length + "_snap";
                canva.width = width;
                canva.height = height;



                myDiv.appendChild(canva);



                var label = document.createElement("label");

                label.style.padding = '2px';
                label.innerHTML = m + ':' + s.toString().split('.')[0];
                label.style.font = "12px arial,serif";

                var hTime = document.createElement("input");
                hTime.setAttribute("type", "hidden");
                hTime.setAttribute("name", "hTime");
                hTime.setAttribute("value", m + ':' + s);

                myDiv.appendChild(label);
                myDiv.appendChild(hTime);
                atg.appendChild(myDiv);
                li.appendChild(atg);

                currentShot = atg;
                changeShotScroll();

                $(atg).contextMenu(menuTag, { triggerOn: 'click', mouseClick: 'right', onOpen: contextOpen });
                $(atg).attr('time', vid.currentTime);

                atg.onclick = clickTags;
                //$('#album').append(canvCrtrWrp);
                // Get a handle on the 2d context of the canvas element
                var context = canva.getContext('2d');
                // Define the size of the rectangle that will be filled (basically the entire element)
                context.fillRect(0, 0, width, height);
                // Grab the image from the video
                context.drawImage(video, 0, 0, width, height);

                cVideoTotalTag = cVideoTotalTag + 1;

                label.id = ($("#button").val() + "_" + cVideoTotalTag + "_time");

                sort(li.id);

                savedata();
            }



        }

        function changeShotScroll() {
            if (currentShot.parentNode.scrollLeft + $(currentShot.parentNode).width() < currentShot.offsetLeft) {
                currentShot.parentNode.scrollLeft = currentShot.offsetLeft;
            } else if (currentShot.parentNode.scrollLeft > currentShot.offsetLeft) {
                currentShot.parentNode.scrollLeft = currentShot.offsetLeft;
            }
        }

        function changeListScroll() {
            var val = $('#button').val();

            var list = $('#' + val + '_file')[0];

            if (list.parentNode.parentNode.parentNode.scrollTop + $(list.parentNode.parentNode.parentNode).height() < list.offsetTop - list.parentNode.parentNode.parentNode.offsetTop) {
                list.parentNode.parentNode.parentNode.scrollTop = list.offsetTop - list.parentNode.parentNode.parentNode.offsetTop;
            } else if (list.parentNode.parentNode.parentNode.scrollTop > list.offsetTop - list.parentNode.parentNode.parentNode.offsetTop) {
                list.parentNode.parentNode.parentNode.scrollTop = list.offsetTop - list.parentNode.parentNode.parentNode.offsetTop;
            }

            //list[0].scrollIntoView();

            // document.body.scrollTop = document.documentElement.scrollTop = 0;
        }
        function moveLeft() {
            document.querySelector('video').currentTime = document.querySelector('video').currentTime - 0.1;
            //left();
        }

        function moveRight() {
            document.querySelector('video').currentTime = document.querySelector('video').currentTime + 0.1;
            //right();
        }
        //navigate left

        function setCurrentShot() {
            $('#tagList a').css('border-color', '#ccc');
            $('#tagList a').css('border-width', '1px');


            var min_sec = currentShot.childNodes["0"].childNodes[3].value.split(':');
            var time = Number(min_sec[0]) * 60 + (Number(min_sec[1]));

            $(currentShot).css('border-color', 'red');
            $(currentShot).css('border-width', '2px');
            changeShotScroll();

            document.querySelector('video').currentTime = time;
            if (time == 0)
                document.querySelector('video').pause();
        }
        function left() {
            if (currentShot == null || $(currentShot).prev().length == 0) {
                var curVideo = $('#' + $('#button').val() + '_file');
                var selected = false;

                while (curVideo.prev().length > 0) {
                    curVideo = $(curVideo.prev()[0]);
                    var id = curVideo.attr('id').split('_')[0];

                    if ($('#' + id + '_tag').children().length > 0) {
                        selected = true;
                        curVideo.trigger('click');

                        $(video).on('loadeddata', function () {
                            $(video).off('loadeddata');
                            currentShot = $('#' + id + '_tag').children().last()[0];
                            setCurrentShot();
                            changeListScroll();
                        });

                        break;
                    }
                }
                if (!selected) {
                    alert("No more tag in left");
                }
                return;
            }

            currentShot = $(currentShot).prev()[0];

            setCurrentShot();
        }

        //navigate right
        function right() {
            if (currentShot != null && $(currentShot).next().length == 0) {
                var curVideo = $('#' + $('#button').val() + '_file');
                var selected = false;

                while (curVideo.next().length > 0) {
                    curVideo = $(curVideo.next()[0]);
                    var id = curVideo.attr('id').split('_')[0];

                    if ($('#' + id + '_tag').children().length > 0) {
                        selected = true;
                        curVideo.trigger('click');

                        $(video).on('loadeddata', function () {
                            $(video).off('loadeddata');
                            currentShot = $('#' + id + '_tag').children()[0];
                            setCurrentShot();
                            changeListScroll();
                        });

                        break;
                    }
                }
                if (!selected) {
                    alert("No more tag in left");
                }
                return;
            }

            if (currentShot == null) {
                if (Number($('#button').val()) > 0 && $('#' + $('#button').val() + "_tag").children().length > 0) {
                    currentShot = $('#' + $('#button').val() + "_tag").children()[0];
                } else {
                    alert("No more tag in right");
                    return;
                }
            } else {
                currentShot = $(currentShot).next()[0];
            }

            setCurrentShot();

            /* if (cVideoCurrent == cVideoTotalTag) {
                alert("No more tag in right");
            }
            else {
                cVideoCurrent = cVideoCurrent + 1;
                var lableid = ($("#button").val() + "_" + cVideoCurrent + "_time");
                var ms = document.getElementById(lableid).textContent.split(':');
                videoStartTime = ms[0] * 60 + ms[1];

                var vid = document.querySelector('video');
                vid.src = vid.src;

            } */
        }

        function spanClick() {
            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);

            currentcount--;

            document.getElementById('needCount').textContent = parseInt(document.getElementById('needCount').textContent) + 1;
            document.getElementById('avaiCount').textContent = parseInt(document.getElementById('avaiCount').textContent) + 1;

            return false;
        }

        function clickTags() {
            document.getElementById('lblstatus').innerHTML = "Completed";
            console.log($(this));
            var val = $('#button').val();

            if ($(this).parent().attr('id').split('_')[0] == val) {
                currentShot = this;
                setCurrentShot();
            } else {
                var id = $(this).parent().attr('id').split('_')[0];
                var file = $('#' + id + '_file');
                file.trigger('click');


                var _this = this;
                $(video).on('loadeddata', function () {
                    $(video).off('loadeddata');
                    currentShot = _this;
                    setCurrentShot();
                });

            }
        }


        function sort(divid) {
            var list = document.getElementById(divid);

            var items = list.childNodes;
            var itemsArr = [];
            for (var i in items) {
                if (items[i].nodeType == 1) { // get rid of the whitespace text nodes
                    itemsArr.push(items[i]);
                }
            }

            itemsArr.sort(function (a, b) {
                return a.childNodes["0"].childNodes[2].innerHTML - b.childNodes["0"].childNodes[2].innerHTML
                    ? 0
                    : (a.childNodes["0"].childNodes[2].innerHTML > b.childNodes["0"].childNodes[2].innerHTML ? 1 : -1);
            });

            for (i = 0; i < itemsArr.length; ++i) {
                list.appendChild(itemsArr[i]);
            }
        }


        function captureAsCanvas(video, options, handle) {
            console.log('begin captureAsCanvas: ' + video.src);

            // Create canvas and call handle function
            var callback = function () {
                var number = options.number;
                var time = options.time;
                var option = options.option;
                var min_sec = options.min_sec;

                var ul = document.getElementById("tagList");
                var vid = document.querySelector('video');

                var li = document.getElementById(number + "_tag");
                li.style.height = '140px';
                li.style.overflowX = 'auto';
                li.style.overflowY = 'hidden';


                if (number == 13) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1750;
                        //  alert("3");
                    }, false);
                }
                if (number == 12) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1600;
                        //  alert("3");
                    }, false);
                }
                if (number == 11) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1450;
                        //  alert("3");
                    }, false);
                }
                if (number == 10) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1300;
                        //  alert("3");
                    }, false);
                }
                if (number == 9) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1150;
                        //  alert("3");
                    }, false);
                }
                if (number == 8) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 1000;
                        //  alert("3");
                    }, false);
                }
                if (number == 7) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 850;
                        //  alert("3");
                    }, false);
                }
                if (number == 6) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 700;
                        //  alert("3");
                    }, false);
                }
                if (number == 5) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 550;
                        //  alert("3");
                    }, false);
                }
                if (number == 4) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 400;
                        //  alert("3");
                    }, false);
                }
                if (number == 3) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 250;
                        //  alert("3");
                    }, false);
                }

                else if (number == 2) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 100;
                        //  alert("2");
                    }, false);
                }

                else if (number == 1) {
                    li.addEventListener("click", function () {

                        var myDiv = document.getElementById('divtaglist');
                        var x = $(li).position();

                        myDiv.scrollTop = 0;
                        // alert("1");
                    }, false);
                }

                // li.style.width = '4000px';

                var m = Number(min_sec[0]);
                var s = (min_sec[1]);

                if (s < 10) {
                    s = '0' + s;
                }

                //check if tag time already exist or not 

                var items = li.childNodes;
                var itemsArr = [];
                for (var i in items) {
                    if (items[i].nodeType == 1) { // get rid of the whitespace text nodes
                        itemsArr.push(items[i]);
                    }
                }


                for (var i in itemsArr) {
                    var ms = m + ':' + s.toString().split('.')[0];

                    if (itemsArr[i].childNodes["0"].childNodes[2].innerHTML == ms) {
                        return false;
                    }
                }
                currentcount++;

                document.getElementById('needCount').textContent = document.getElementById('needCount').textContent - 1;
                document.getElementById('avaiCount').textContent = document.getElementById('avaiCount').textContent - 1;
                var atg = document.createElement("a");
                atg.className = "fragment";
                //atg.onclick = alert('no man!');




                var myDiv = document.createElement("div");
                var sp = document.createElement('span');
                sp.innerHTML = 'X';
                sp.id = 'close';
                sp.onclick = spanClick;

                myDiv.style.height = '90px';
                myDiv.style.cssFloat = "left";
                myDiv.style.padding = '10px';
                myDiv.appendChild(sp);

                ratio = video.videoWidth / video.videoHeight;

                height = 70;
                width = parseInt(height * ratio, 10);

                var canva = document.createElement("canvas");
                canva.id = li.id + "_" + li.childNodes.length + "_snap";
                canva.width = width;
                canva.height = height;

                myDiv.appendChild(canva);

                var label = document.createElement("label");

                label.style.padding = '2px';
                label.innerHTML = m + ':' + s.toString().split('.')[0];
                label.style.font = "12px arial,serif";

                var hTime = document.createElement("input");
                hTime.setAttribute("type", "hidden");
                hTime.setAttribute("name", "hTime");
                hTime.setAttribute("value", m + ':' + s);

                myDiv.appendChild(label);
                myDiv.appendChild(hTime);
                atg.appendChild(myDiv);
                li.appendChild(atg);

                $(atg).contextMenu(menuTag, { triggerOn: 'click', mouseClick: 'right', onOpen: contextOpen });
                atg.onclick = clickTags;
                //$('#album').append(canvCrtrWrp);
                // Get a handle on the 2d context of the canvas element
                var context = canva.getContext('2d');
                // Define the size of the rectangle that will be filled (basically the entire element)
                context.fillRect(0, 0, width, height);
                // Grab the image from the video
                context.drawImage(video, 0, 0, width, height);

                cVideoTotalTag = cVideoTotalTag + 1;

                label.id = (number + "_" + cVideoTotalTag + "_time");

                $(atg).attr('time', time);

                var confs = option.split('*');

                for (var i = 0; i < confs.length; i++) {
                    if (confs[i] == "1") {
                        $(atg).attr(configs[i], 1);
                    } else {
                        $(atg).attr(configs[i], 0);
                    }
                }
                sort(li.id);

                $(video).unbind('seeked');
                // Call handle function (because of event)
                handle.call(this, canvas);
            }

            // If we have time in options 
            if (options.time >= 0) {
                // Seek to any other time
                if (options.time == 0)
                    video.currentTime = 0.001;
                else
                    video.currentTime = options.time;
                // Wait for seeked event
                $(video).bind('seeked', callback);
                console.log('end1 captureAsCanvas: ' + video.src);
                return;
            }

            // Otherwise callback with video context - just for compatibility with calling in the seeked event
            console.log('end2 captureAsCanvas: ' + video.src);
            return;
        }

        function loadVideo(video, src, handle) {
            console.log('begin loadVideo: ' + src);
            var callback = function () {
                $(video).unbind('loadeddata');
                handle.call(this);
            }

            video.src = src;
            $(video).bind('loadeddata', callback);
            console.log('end loadVideo: ' + src);
            return;
        }

        function loadShot(list, index) {
            console.log('begin loadShot' + index);
            document.getElementById("lblstatus").innerHTML = "Loading Tags";
            if (index > list.length - 1) {
                document.getElementById("lblstatus").innerHTML = "Tag Loading Completed";
                return;
            }

            var item = list[index].split(",");

            var number = item[0];

            var time = item[1];
            var min_sec = new Array();
            min_sec[0] = Math.floor(time / 60);
            min_sec[1] = time % 60;
            //min_sec[1] = Math.floor(time % 60);
            //min_sec[1] = Math.floor(time);
            console.log('loadShot' + index + ': time=' + time);
            console.log('loadShot' + index + ': min_sec=' + min_sec);

            var option = item[2];

            var video1 = document.querySelector('#videoarea1');

            if (video1.src != $('#' + number + '_file').attr('movieurl')) {
                video1.src = $('#' + number + '_file').attr('movieurl');

                loadVideo(video1, $('#' + number + '_file').attr('movieurl'), function () {
                    captureAsCanvas(video1, { number: number, time: time, option: option, min_sec: min_sec }, function (canvas) {
                        loadShot(list, index + 1);
                    });
                });
            } else {
                captureAsCanvas(video1, { number: number, time: time, option: option, min_sec: min_sec }, function (canvas) {
                    loadShot(list, index + 1);
                });
            }
            console.log('complete loadShot' + index);
        }

        $.ajax({
            type: "POST",
            url: "Dashboard.aspx/loadData",
            data: {},
            contentType: "application/json; charset=utf-8",
            data: "{'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "'}",
            success: function (response) {
                if (response.d != "") {
                    var list = response.d.split("_");
                    console.log('entering loadShot');
                    loadShot(list, 0);
                    console.log('exiting loadShot');
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });

        function minmax(value, min, max) {
            if (parseInt(value) < min || isNaN(parseInt(value)))
                return min;
            else if (parseInt(value) > max)
                return max;
            else return value;
        }


        $(document).ready(function () {
            var $myDiv = $('#divtaglist');
            var mainHeight = $('#main')[0].scrollHeight;
            if (mainHeight != 400) {
                var st = mainHeight + 400;
                $myDiv.height(st);
                if (st == 0) {
                    $myDiv.hide();
                } else {
                    $myDiv.show();
                }
            }
        });



        function PassArraytoCode() {

            
            var theIds = JSON.stringify(regionArr);

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'Dashboard.aspx/PassThings',
                data: "{'ids':'" + theIds + "'}",
                
                success: function () {
                    
                },
                error: function (response) {
                    alert(JSON.stringify(response));
                }
            });
        }

    </script>

</body>
</html>
