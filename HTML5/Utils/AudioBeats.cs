﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTML5.Utils
{
    public class AudioBeats
    {
        public static string detectAudioBeats(String musicFileName)
        {
            //String script = @"C:\Python\myscripts\detectMusicTrackBeats.py";
            // Run beat detector via Python
            //System.Diagnostics.Process p1 = System.Diagnostics.Process.Start(@"py", script + " \"" + musicFileName + "\"");

            //// Wait up to 5 minutes for the script to complete
            //p1.WaitForExit(1000 * 60 * 5);

            return System.IO.Path.GetDirectoryName(musicFileName) + @"\beat_times.csv";
        }
    }
}