﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="HTML5.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<script src="jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Load the FileUpload Plugin (more info @ https://github.com/blueimp/jQuery-File-Upload) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.14.1/js/jquery.fileupload.min.js"></script>


<script>
    var mydata = [];
    $(document).ready(function () {

        // Assigned to variable for later use.
        var form = $('.direct-upload');
        var filesUploaded = [];
        // Place any uploads within the descending folders
        // so ['test1', 'test2'] would become /test1/test2/filename
        var folders = [];
        form.fileupload({
            url: form.attr('action'),
            type: form.attr('method'),
            datatype: 'xml',
            add: function (event, data) {
                // Show warning message if your leaving the page during an upload.
                window.onbeforeunload = function () {
                    return 'You have unsaved changes.';
                };
                // Give the file which is being uploaded it's current content-type (It doesn't retain it otherwise)
                var file = data.files[0];
                var user = document.getElementById('txtusername').value;
                var project = document.getElementById('txtprojectname').value;
                var filename = user + "/" + project + "/" + file.name;
                form.find('input[name="Content-Type"]').val(file.type);
                form.find('input[name="key"]').val((folders.length ? folders.join('/') + '/' : '') + filename);
                // Actually submit to form to S3.
                data.submit();
                // Show the progress bar
                // Uses the file size as a unique identifier
                var bar = $('<div class="progress" data-mod="' + file.size + '"><div class="bar"></div></div>');
                $('.progress-bar-area').append(bar);
                bar.slideDown('fast');
            },
            progress: function (e, data) {
                // This is what makes everything really cool, thanks to that callback
                // you can now update the progress bar based on the upload progress.
                var percent = Math.round((data.loaded / data.total) * 100);
                $('.progress[data-mod="' + data.files[0].size + '"] .bar').css('width', percent + '%').html(percent + '%');
            },
            fail: function (e, data) {
                // Remove the 'unsaved changes' message.
                window.onbeforeunload = null;
                $('.progress[data-mod="' + data.files[0].size + '"] .bar').css('width', '100%').addClass('red').html('');
            },
            done: function (event, data) {
                window.onbeforeunload = null;
                // Upload Complete, show information about the upload in a textarea
                // from here you can do what you want as the file is on S3
                // e.g. save reference to your server using another ajax call or log it, etc.
                var original = data.files[0];
                var s3Result = data.result.documentElement.children;
                filesUploaded.push({
                    "original_name": original.name,
                    "s3_name": s3Result[2].innerHTML,
                    "size": original.size,
                    "url": s3Result[0].innerHTML.replace("%2F", "/").replace("%2F", "/")
                });
                $('#uploaded').html(JSON.stringify(filesUploaded, null, 2));
                mydata = filesUploaded;
            }
        });
    });

    function ConfirmAndExecute() {


        if (confirm("Matched files :- " + document.getElementById("HiddenField1").value + "!! \n \n Do want to continue ?")) {
            var test1 = document.getElementById("txtusername").value;
            var test2 = document.getElementById("txtprojectname").value;
            window.location = "Dashboard.aspx?Usr=" + test1 + "&pro=" + test2;
        }
        else {
            document.getElementById("selectedFiles").innerHTML = "";
            document.getElementById("hdn").value = "";
            document.getElementById("id_upload").value = "";
        }

    }


    function CreateProjectFolder() {
        if (confirm("Your project folder does not exist !! Do want to continue to create ?")) {
            var usr = document.getElementById("txtusername").value;
            var prj = document.getElementById("txtprojectname").value;

            $.ajax({
                type: "POST",
                url: "Home.aspx/CreateBucket",
                data: {},
                contentType: "application/json; charset=utf-8",
                data: "{'user':'" + document.getElementById('txtusername').value + "','project':'" + document.getElementById('txtprojectname').value + "'}",
                success: function (response) {
                    if (response.d != "") {
                        var test1 = document.getElementById("lblMessage");
                        test1.style.color = "green";
                        test1.innerHTML = "Folder created successfully. Please upload the files."
                        document.getElementById('id_upload').style.visibility = 'visible';
                        document.getElementById('btnsubmit').style.visibility = 'hidden';

                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });


        }
    }

    function handleFileSelect() {
        'use strict'
        var URL = window.URL || window.webkitURL

        var elem = document.getElementById("id_upload");
        var names = [];
        var fileURLs = [];
        var selDiv = document.getElementById("selectedFiles");
        selDiv.innerHTML = "";
        for (var i = 0; i < elem.files.length; ++i) {
            names.push(elem.files[i].name);
            fileURLs.push(URL.createObjectURL(elem.files[i]));



            var hdn = document.getElementById("hdn");
            if (!selDiv.innerHTML.includes(elem.files[i].name)) {
                selDiv.innerHTML += elem.files[i].name + "<br/>"
                hdn.value += "~" + elem.files[i].name;
            }
        }
    }

    function redirecttodashboard() {
        $.ajax({
            type: "POST",
            url: "Home.aspx/btnSubmit_Click",            
            contentType: "application/json; charset=utf-8",
            data: "{'username':'" + document.getElementById('txtusername').value + "','projectname':'" + document.getElementById('txtprojectname').value + "','uploadedfiles':'" + JSON.stringify( mydata ) + "'}",
            dataType: "json",
            success: function (response) {
                if (response.d != "") {
                    window.location = "../Dashboard.aspx?Usr=" + document.getElementById('txtusername').value + "&pro=" + document.getElementById('txtprojectname').value;
                }
            },
            failure: function (response) {
                alert(response);
            }
        });
    }
    </script>
<script src="jquery.repeater.js"></script>
<body>
    <head runat="server">


        <title>Home</title>
        <link href="Style.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" />
    </head>

    <!--<script src="jquery-3.2.1.min.js"></script>-->
    <!-- Start of the JavaScript -->
    <!-- Load jQuery & jQuery UI (Needed for the FileUpload Plugin) -->

    <div style="top: 20%; left: 40%; width: 500px; position: absolute;">
        <form id="homepage" runat="server" method="POST" enctype="multipart/form-data" class="direct-upload">

            <h1>Video Editor</h1>
            <asp:HiddenField ID="hdn" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
            <table class="tblmain">
                <tr>

                    <td title="Username Help Text">Username
                    </td>
                    <td>
                        <asp:TextBox ID="txtusername" title="username field help text" Text="user" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtusername" ValidationGroup="ab" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td title="Project Name Help Text">Project Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtprojectname" title="projectname field help text" Text="project" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtprojectname" ValidationGroup="ab" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>

                </tr>
            </table>
            <table class="tblmain2">
                <tr>
                    <td colspan="2">
                        <div id="selectedFiles"></div>

                    </td>
                </tr>
                <tr>
                    <div id="box" runat="server">
                        <td colspan="2">
                            <asp:Button ID="id_upload" title="Upload Files field Help Text" runat="server" Text="Upload Files" OnClick="btnUpload_Click" />
                        </td>
                    </div>
                </tr>
                <tr>
                    <!-- Progress Bars to show upload completion percentage -->
                    <div class="progress-bar-area"></div>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HyperLink ID="btnUpload" title="Submit button help text" runat="server" Text="Submit" NavigateUrl="javascript:redirecttodashboard();"></asp:HyperLink>
                    </td>
                    <label id="lblMessage"></label>
                </tr>

                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnsubmit" runat="server" title="validate button Help Text" Text="Validate" ValidationGroup="ab" OnClick="btnValidate_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span id="Span1" runat="server" style="color: red"></span>
                    </td>
                </tr>
            </table>

        </form>
        <div>
            <h3>Files</h3>
            <textarea id="uploaded" title="Uploaded files area help text"></textarea>
        </div>
    </div>

</body>
</html>
