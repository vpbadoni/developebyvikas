﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using Amazon.S3.Transfer;

namespace HTML5
{
    public partial class Dashboard : System.Web.UI.Page
    {
        int count = 0;

        public string accessKey = System.Configuration.ConfigurationManager.AppSettings["accessKey"];
        public string secretKey = System.Configuration.ConfigurationManager.AppSettings["secretKey"];
        public string rootBucket = System.Configuration.ConfigurationManager.AppSettings["rootBucket"];
        public AmazonS3Config config = new AmazonS3Config();
        List<string> filename = new List<string>();
        static string[] words;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                hdnuser.Value = Request.QueryString["Usr"].ToString();
                hdnproject.Value = Request.QueryString["pro"].ToString();
                try
                {

                    List<string> fileNamesVideo = new List<string>();
                    List<string> fileNamesMp3 = new List<string>();
                    List<string> finalObjList = new List<string>();


                    string path = @"C:\VideoEditor\Projects\" + hdnuser.Value + "\\" + hdnproject.Value + "\\" + "TrackListConfig.csv";

                    int len = 0;
                    if (File.Exists(path))
                    {
                        string[] readText = File.ReadAllLines(path);
                        len = readText.Length;
                    }
                    // This text is added only once to the file.
                    if (File.Exists(path) && len > 1)
                    {
                        string result = "";
                        try
                        {
                            // Open the file to read from.
                            string[] readText = File.ReadAllLines(path);
                            int i = 0;
                            foreach (string s in readText)
                            {
                                if (i == 0)
                                {
                                    i = 1;
                                    continue;
                                }

                                // Skip any blank lines
                                if (s.Equals(""))
                                    continue;

                                string[] items = s.Split(',');
                                //   string[] items = Session["fileUrl"].ToString().Split('~');

                                string filename = items[1];

                                string[] fileext = filename.Split('.');

                                if (fileext[1] == "MP3" || fileext[1] == "mp3")
                                {
                                    string strfilename = items[1];
                                    string strfileurl = items[2];
                                    BindMP3File(strfilename, strfileurl);
                                }
                                else
                                {
                                    HtmlGenericControl li1 = new HtmlGenericControl("li");
                                    li1.ID = items[0] + "_file";
                                    li1.Attributes.Add("class", "list-group-item");
                                    // li1.InnerText = "sintel";
                                    li1.InnerText = items[1];
                                    li1.Style.Add("height", "140px");
                                    li1.Style.Add("width", "100%");
                                    li1.Attributes.Add("movieurl", items[2]);
                                    string option = items.Length > 3 ? items[3] : "";
                                    if (!String.IsNullOrEmpty(option))
                                    {
                                        if (String.Compare(option, "reverse") == 0)
                                        {
                                            li1.Attributes.Add("reverse", "1");
                                        }
                                        else if (String.Compare(option, "flip") == 0)
                                        {
                                            li1.Attributes.Add("flip", "1");
                                        }
                                        else
                                        {
                                            li1.Attributes.Add("reverse", "1");
                                            li1.Attributes.Add("flip", "1");
                                        }
                                    }
                                    //li1.Attributes.Add("movieurl", response.S3Objects[index].ETag); 
                                    playlist.Controls.Add(li1);

                                    HtmlGenericControl litag1 = new HtmlGenericControl("li");
                                    litag1.ID = items[0] + "_tag";
                                    litag1.Attributes.Add("class", "list-group-item");
                                    litag1.Attributes.Add("title", " configure marker");
                                    tagList.Controls.Add(litag1);
                                    count++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        foreach (var item in finalObjList)
                        {

                            HtmlGenericControl li = new HtmlGenericControl("li");
                            li.ID = count + 1 + "_file";
                            li.Attributes.Add("class", "list-group-item");
                            li.Attributes.Add("height", "90px");
                            // li.InnerText = item.Key;
                            playlist.Controls.Add(li);

                            HtmlGenericControl litag = new HtmlGenericControl("li");
                            litag.ID = count + 1 + "_tag";
                            litag.Attributes.Add("class", "list-group-item");
                            tagList.Controls.Add(litag);


                            count++;

                        }

                        //int index = response.S3Objects.FindIndex(a => a.Key == item);

                        //HtmlGenericControl li1 = new HtmlGenericControl("li");
                        //li1.ID = count + 1 + "_file";
                        //li1.Attributes.Add("class", "list-group-item");
                        //li1.InnerText = "sintel";
                        //li1.InnerText = item;
                        //li1.Style.Add("height", "140px");
                        //li1.Style.Add("width", "100px");
                        //li1.Attributes.Add("movieurl", "http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4");
                        ////li1.Attributes.Add("movieurl", response.S3Objects[index].ETag); 
                        //playlist.Controls.Add(li1);

                        //HtmlGenericControl litag1 = new HtmlGenericControl("li");
                        //litag1.ID = count + 1 + "_tag";
                        //litag1.Attributes.Add("class", "list-group-item");
                        //litag1.Attributes.Add("title", " configure marker");
                        //tagList.Controls.Add(litag1);
                        //count++;

                    }

                    string pathConfig = @"C:\VideoEditor\Projects\" + hdnuser.Value + "\\" + hdnproject.Value + "\\" + "Config.csv";

                    int leng = 0;
                    if (File.Exists(pathConfig))
                    {
                        string[] readTextConfig = File.ReadAllLines(pathConfig);
                        leng = readTextConfig.Length;
                    }


                    if (File.Exists(pathConfig) && leng > 1)
                    {
                        string resultConfig = "";
                        try
                        {
                            // Open the file to read from.
                            string[] readText = File.ReadAllLines(pathConfig);
                            int i = 0;
                            foreach (string s in readText)
                            {
                                if (i == 0)
                                {
                                    i = 1;
                                    continue;
                                }

                                // Skip any blank lines
                                if (s.Equals(""))
                                    continue;

                                string[] itemsConfig = s.Split(',');
                                if (itemsConfig.Length > 0)
                                {
                                    if (itemsConfig[0] != "undefined" && itemsConfig[0] != "")
                                    {
                                        optradio.SelectedValue = itemsConfig[0];
                                    }
                                    txtTrantime.Value = (itemsConfig[1] != "") ? itemsConfig[1] : "350ms";
                                    txtdecimator.Value = (itemsConfig[2] != "") ? itemsConfig[2] : "";
                                    txtplayback.Value = (itemsConfig[3] != "") ? itemsConfig[3] : "";
                                    optradioselmethod.SelectedValue = (itemsConfig[4] != "undefined" && itemsConfig[4] != "") ? itemsConfig[4] : "User Input";
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }


                    //< li class="list-group-item " id="2" movieurl="http://html5videoformatconverter.com/data/images/happyfit2.mp4" moviesposter="http://html5videoformatconverter.com/data/images/screen.jpg">Happy Fit</li>
                    //         <li class="list-group-item" id="2" movieurl="http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4">Sintel</li>
                    //         <li class="list-group-item" movieurl="http://html5example.net/static/video/html5_Video_VP8.webm">Resident Evil</li>
                    //         <li class="list-group-item" movieurl="http://www.ioncannon.net/examples/vp8-webm/big_buck_bunny_480p.webm">Big Buck Bunny</li>
                }

                catch (Exception ex)
                {

                }
            }

           
        }


        [WebMethod]
        public static void PassThings(string ids)
        {
            string t = ids.ToString();
            words = ids.Split(',');
            
        }

        public void BindMP3File(string strfilename,string strfileurl)
        {
            
            //string strUser  = Request.QueryString["Usr"].ToString();
            //string strProject = Request.QueryString["pro"].ToString();

            //string path = @"C:\VideoEditor\Projects\" + strUser + "\\" + strProject + "\\" + "TrackListConfig.csv";

            //int len = 0;
            //if (File.Exists(path))
            //{
            //    string[] readText = File.ReadAllLines(path);
            //    len = readText.Length;
            //}

            //if (File.Exists(path) && len > 1)
            //{
            //    string result = "";
            //    try
            //    {
            //        // Open the file to read from.
            //        string[] readText = File.ReadAllLines(path);
            //        int i = 0;
            //        foreach (string s in readText)
            //        {
            //            if (i == 0)
            //            {
            //                i = 1;
            //                continue;
            //            }

            //            // Skip any blank lines
            //            if (s.Equals(""))
            //                continue;

            //            string[] items = s.Split(',');
            //            //   string[] items = Session["fileUrl"].ToString().Split('~');

                        string script = "window.onload = function() { getSonglist('" + strfilename + "," + strfileurl + "'); };";
                        ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", script, true);

                        //HtmlGenericControl li1 = new HtmlGenericControl("li");
                        //li1.ID = items[0] + "_file";
                        //li1.Attributes.Add("class", "list-group-item");
                        //// li1.InnerText = "sintel";
                        //li1.InnerText = items[1];
                        //li1.Style.Add("height", "140px");
                        //li1.Style.Add("width", "100px");
                        //li1.Attributes.Add("movieurl", items[2]);
                        //string option = items.Length > 3 ? items[3] : "";
                        //if (!String.IsNullOrEmpty(option))
                        //{
                        //    if (String.Compare(option, "reverse") == 0)
                        //    {
                        //        li1.Attributes.Add("reverse", "1");
                        //    }
                        //    else if (String.Compare(option, "flip") == 0)
                        //    {
                        //        li1.Attributes.Add("flip", "1");
                        //    }
                        //    else
                        //    {
                        //        li1.Attributes.Add("reverse", "1");
                        //        li1.Attributes.Add("flip", "1");
                        //    }
                        //}
                        ////li1.Attributes.Add("movieurl", response.S3Objects[index].ETag); 
                        //playlist.Controls.Add(li1);

                        //HtmlGenericControl litag1 = new HtmlGenericControl("li");
                        //litag1.ID = items[0] + "_tag";
                        //litag1.Attributes.Add("class", "list-group-item");
                        //litag1.Attributes.Add("title", " configure marker");
                        //tagList.Controls.Add(litag1);
                        //count++;

                       
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}



        }




        [WebMethod]

        public static void downloadFile(string user, string project)
        {
            try
            {
                AmazonS3Config config = new AmazonS3Config();
                AmazonS3Client client = new AmazonS3Client(
                    System.Configuration.ConfigurationManager.AppSettings["accessKey"],
                    System.Configuration.ConfigurationManager.AppSettings["secretKey"], Amazon.RegionEndpoint.USEast1
                    );


                TransferUtility utility = new TransferUtility(client);
                // making a TransferUtilityUploadRequest instance
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();


                request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"] + "/" + user + "/" + project;
                request.Key = "TrackListConfig.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TrackListConfig.csv"; //local file name
                utility.Upload(request); //commensing the transfer

                request.Key = "TagListConfig.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TagListConfig.csv"; //local file name
                utility.Upload(request); //commensing the transfer

                request.Key = "Config.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "Config.csv"; //local file name
                utility.Upload(request); //commensing the transfer
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]

        public static void SaveDataTos3(string json, string user, string project)
        {

            try
            {
                AmazonS3Config config = new AmazonS3Config();
                AmazonS3Client client = new AmazonS3Client(
                    System.Configuration.ConfigurationManager.AppSettings["accessKey"],
                    System.Configuration.ConfigurationManager.AppSettings["secretKey"], Amazon.RegionEndpoint.USEast1
                    );


                TransferUtility utility = new TransferUtility(client);
                // making a TransferUtilityUploadRequest instance
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();


                request.BucketName = System.Configuration.ConfigurationManager.AppSettings["rootBucket"] + "/" + user + "/" + project;
                request.Key = "TrackListConfig.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TrackListConfig.csv"; //local file name
                utility.Upload(request); //commensing the transfer

                request.Key = "TagListConfig.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TagListConfig.csv"; //local file name
                utility.Upload(request); //commensing the transfer

                request.Key = "Config.csv"; //file name up in S3
                request.FilePath = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "Config.csv"; //local file name
                utility.Upload(request); //commensing the transfer
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]

        public static string saveData(string json, string user, string project)
        {
            try
            {
                List<string> obj = JsonConvert.DeserializeObject<List<string>>(json);
                Dashboard objdash = new Dashboard();

                using (var stream = File.CreateText(@"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TrackListConfig.csv"))
                {
                    string csvRow = string.Format("{0},{1},{2},{3}", "MediaFileIndex", "FileName", "Url", "Config");

                    stream.WriteLine(csvRow);

                    for (int i = 0; i < (obj.Count() - 1); i++)
                    {
                        List<string> details = obj[i].Split(new string[] { "@@@" }, StringSplitOptions.None).ToList();

                        string[] infos = details[3].Split('_');
                        string conf = "";
                        if (String.Compare(infos[0], "1") == 0)
                        {
                            conf += "reverse";
                        }
                        if (String.Compare(infos[1], "1") == 0)
                        {
                            if (conf != "")
                                conf += " ";
                            conf += "flip";
                        }

                        csvRow = string.Format("{0},{1},{2},{3}", details[0], details[1], details[2], conf);


                        stream.WriteLine(csvRow);
                    }
                }
                using (var stream = File.CreateText(@"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TagListConfig.csv"))
                {
                    string csvRow = string.Format("{0},{1},{2}", "MediaFileIndex", "TimeCode", "Configuration");

                    stream.WriteLine(csvRow);

                    for (int i = 0; i < (obj.Count() - 1); i++)
                    {
                        List<string> details = obj[i].Split(new string[] { "@@@" }, StringSplitOptions.None).ToList();
                        string[] tags = { };
                        if (!string.IsNullOrEmpty(details[4]))
                        {
                            tags = details[4].Split(',');
                        }

                        int j = i + 1;
                        if (tags.Length > 0)
                        {
                            string[] infos = { };
                            foreach (string tag in tags)
                            {
                                infos = tag.Split('_');
                                string[] confs = infos[2].Split('*');

                                /*if (String.Compare(infos[2], "1") == 0)
                                {
                                    conf += "reverse";
                                }
                                if (String.Compare(infos[3], "1") == 0)
                                {
                                    if (conf != "")
                                        conf += " ";
                                    conf += "flip";
                                }*/
                                csvRow = string.Format("{0},{1},{2}", infos[0], infos[1], infos[2]);
                                stream.WriteLine(csvRow);
                            }
                        }
                    }
                }
                string pathconfig = @"C:\VideoEditor\Projects\" + user + "\\" + project;
                bool folderExists = Directory.Exists(pathconfig);


                if (!folderExists)
                    Directory.CreateDirectory(pathconfig);
                using (var stream = File.CreateText(pathconfig + "\\Config.csv"))
                {
                    string csvRow = string.Format("{0},{1},{2},{3},{4}", "ClipOrder", "TransitionTime", "Decimator", "Playback Rate", "SelectionMethod");

                    stream.WriteLine(csvRow);

                    List<string> details = obj[obj.Count - 1].Split(new string[] { "@@@" }, StringSplitOptions.None).ToList();

                    csvRow = string.Format("{0},{1},{2},{3},{4}", details[0], details[1], details[2], details[3], details[4]);
                    stream.WriteLine(csvRow);


                }
            }
            catch (Exception ex)
            {

            }

            return "success";
        }

        [WebMethod]

        public static string loadData(string user, string project)
        {
            string result = "";
            try
            {
                string path = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "TagListConfig.csv";

                // This text is added only once to the file.
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    return "";
                }

                // Open the file to read from.
                string[] readText = File.ReadAllLines(path);
                int i = 0;
                foreach (string s in readText)
                {
                    if (i == 0)
                    {
                        i = 1;
                        continue;
                    }
                    result += "_" + s;
                }
            }
            catch (Exception ex)
            {

            }

            if (result != "")
                return result.Substring(1);
            else return "";
        }

        [WebMethod]

        public static string submitRender(string json, string user, string project)
        {
            using (var stream = File.CreateText(@"C:\VideoEditor\Projects\processing_queue\pending" + "\\" + user + "_" + project + "_" + "project.csv"))
            {
                string csvRow = string.Format("{0},{1}", "User", "Project");
                stream.WriteLine(csvRow);
                csvRow = string.Format("{0},{1}", user, project);
                stream.WriteLine(csvRow);
            }

            return "In Progress";
        }

        [WebMethod]

        public static string SetInitialCount(string user, string project, string decimator)
        {
            
            int min = 50;
            int max = 100;
            try
            {


                // Hard code it because Dashboard.aspx is not taking it from Config.csv yet
                decimator = "8";

                int audioBeatDecimator = Convert.ToInt16(decimator);

                String musicFileName = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + getMusicFile(user, project);
                if (!File.Exists(musicFileName))
                    throw new System.ArgumentException("Cannot calculate beats, music file does not exist: ", musicFileName);

                String beatsFile = @"C:\VideoEditor\Projects\" + user + "\\" + project + "\\" + "beat_times.csv";
                if (!File.Exists(beatsFile))
                    beatsFile = HTML5.Utils.AudioBeats.detectAudioBeats(musicFileName);

                // Determine the maximum number of tags
                var numberOfLines = File.ReadAllLines(beatsFile).Length;
                var beatCount = numberOfLines / 2;      // for some reason there is an empty line between each line
                var numberOfTags = beatCount / audioBeatDecimator;
                max = numberOfTags;
                min = numberOfTags / 2;
            }
            catch(Exception ex)
            {

            }

            return min + "," + max;
        }

        [Serializable]
        public class PostOrderViewModel
        {
            public Array[] json { get; set; }
        }

        protected void useLocalFiles(object sender, EventArgs e)
        {

        }

        public Boolean CheckS3ProjectFiles(string usrName, string prjName)
        {
            Boolean fileStatus = false;
            using (IAmazonS3 s3Client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1))
            {
                try
                {
                    // S3DirectoryInfo s3DirectoryInfo = new Amazon.S3.IO.S3DirectoryInfo(s3Client, rootBucket + "/" + usrName + "/" + prjName);

                    AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1);

                    ListObjectsRequest request = new ListObjectsRequest();
                    request.BucketName = rootBucket;
                    request.Prefix = usrName + "/" + prjName + "/";
                    ListObjectsResponse response = client.ListObjects(request);

                    var count = response.S3Objects.Count;
                    if (count > 0)
                    {
                        fileStatus = true;
                    }
                    else
                    {
                        fileStatus = false;
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    fileStatus = false;
                }

            }

            return fileStatus;
        }

        protected void submitFiles_Click(object sender, EventArgs e)
        {
             string[] uploadedfiles = hdn.Value.Split('~');
            string userName = hdnuser.Value;
            string projectName = hdnproject.Value;

            HiddenField1.Value = "";
            string path = @"C:\VideoEditor\Projects\" + userName + @"\" + projectName + @"\" + "TrackListConfig.csv";


            if (File.Exists(path))
            {
                string result = "";
                try
                {
                    // Open the file to read from.
                    string[] readText = File.ReadAllLines(path);
                    int i = 0;
                    foreach (string s in readText)
                    {
                        if (i == 0)
                        {
                            i = 1;
                            continue;
                        }

                        // Skip any blank lines
                        if (s.Equals(""))
                            continue;

                        string[] items = s.Split(',');
                        filename.Add(items[1]);

                    }
                }
                catch (Exception ex)
                {

                }
            }
            string[] arryfiles = filename.ToArray();
            bool containsValues = arryfiles.Intersect(uploadedfiles).Any();

            if (containsValues)
            {
                for (var i = 0; i < arryfiles.Length; i++)
                {
                    for (var j = 0; j < uploadedfiles.Length; j++)
                    {
                        if (arryfiles[i] == uploadedfiles[j])
                        {
                            HiddenField1.Value += "\n" + uploadedfiles[j];
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "msgbxConfirm", "ConfirmAndExecute();", true);

                        }
                    }
                    if (HiddenField1.Value.Length > 0)
                    {

                    }
                }
            }
            else
            {
                foreach (var files in uploadedfiles)

                {
                    if(!string.IsNullOrEmpty(files))
                    {
                        string[] items = files.Split('@');
                        //   string[] items = Session["fileUrl"].ToString().Split('~');

                        HtmlGenericControl li1 = new HtmlGenericControl("li");
                        li1.ID = 2 + "_file";
                        li1.Attributes.Add("class", "list-group-item");
                        // li1.InnerText = "sintel";
                        li1.InnerText = items[0];
                        li1.Style.Add("height", "140px");
                        li1.Style.Add("width", "100px");
                        li1.Attributes.Add("movieurl", items[1]);
                        li1.Attributes.Add("local", "Y");
                        //string option = items.Length > 3 ? items[3] : "";
                        //if (!String.IsNullOrEmpty(option))
                        //{
                        //    if (String.Compare(option, "reverse") == 0)
                        //    {
                        //        li1.Attributes.Add("reverse", "1");
                        //    }
                        //    else if (String.Compare(option, "flip") == 0)
                        //    {
                        //        li1.Attributes.Add("flip", "1");
                        //    }
                        //    else
                        //    {
                        //        li1.Attributes.Add("reverse", "1");
                        //        li1.Attributes.Add("flip", "1");
                        //    }
                        //}
                        //li1.Attributes.Add("movieurl", response.S3Objects[index].ETag); 
                        playlist.Controls.Add(li1);
                    }
                }

                
            }
        }

        protected static String getMusicFile(string user, string project)
        {
            string path = @"C:\VideoEditor\Projects\" + user + @"\" + project + @"\" + "TrackListConfig.csv";

            foreach (string file in getFilenames(path))
            {
                if (file.EndsWith(".mp3") || file.EndsWith(".wav"))
                    return file;
            }

            return "";
        }

        protected static List<string> getFilenames(string path)
        {
            List<string> filenames = new List<string>();
            if (File.Exists(path))
            {
                string result = "";
                try
                {
                    // Open the file to read from.
                    string[] readText = File.ReadAllLines(path);
                    int i = 0;
                    foreach (string s in readText)
                    {
                        // Skip the header
                        if (i == 0)
                        {
                            i = 1;
                            continue;
                        }

                        // Skip any blank lines
                        if (s.Equals(""))
                            continue;

                        string[] items = s.Split(',');
                        filenames.Add(items[1]);

                    }
                }
                catch (Exception ex)
                {

                }
            }

            return filenames;
        }
    }
}