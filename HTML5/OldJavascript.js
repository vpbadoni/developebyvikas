﻿

var configs = ['reverse', 'flip', 'first', 'last', 'slow', 'fast', 'envelope', 'effect', 'beginning', 'end'];
var currentcount = 0;
function removeAll(item) {
    var clickedId = item.trigger[0].id;
    var tagId = clickedId.split("_")[0];
    var parent = null;
    if (tagId) {
        if (tagId == $('#button').val()) {
            document.getElementById("videoarea").currentTime = 0;
            document.getElementById("videoarea").pause();

            var parent = $(item.trigger[0]).parent();
        }
        tagId = tagId + "_tag";
        var tag = $('#' + tagId);

        currentcount = currentcount - tag.children().length;

        document.getElementById('needCount').textContent = parseInt(document.getElementById('needCount').textContent) + tag.children().length;
        document.getElementById('avaiCount').textContent = parseInt(document.getElementById('avaiCount').textContent) + tag.children().length;

        tag.remove();
        $(item.trigger[0]).remove();

        if (parent && parent.children()[0]) {
            $(parent.children()[0]).trigger('click');
        }
    }

    savedata();
}

function removeOne(item) {
    var clickedItem = item.trigger[0];

    if (currentShot != null && clickedItem == currentShot) {
        if ($(currentShot).next().length > 0) {
            currentShot = $(currentShot).next()[0];
        } else if ($(currentShot).prev().length > 0) {
            currentShot = $(currentShot).prev()[0];
        } else {
            currentShot = null;
        }

        $(currentShot).css('border-color', 'red');
        $(currentShot).css('border-width', '2px');
        changeShotScroll();
    }
    clickedItem.parentNode.removeChild(clickedItem);

    currentcount--;

    document.getElementById('needCount').textContent = parseInt(document.getElementById('needCount').textContent) + 1;
    document.getElementById('avaiCount').textContent = parseInt(document.getElementById('avaiCount').textContent) + 1;

    savedata();

}

function changeStatus(item, status) {
    var clickedItem = item.trigger[0];

    if ($(clickedItem).attr(status) == '1') {
        $(clickedItem).attr(status, 0);
    } else {
        switch (status) {
            case 'beginning':
                $(clickedItem).attr('end', 0);
                break;
            case 'end':
                $(clickedItem).attr('beginning', 0);
                break;
            case 'slow':
                $(clickedItem).attr('fast', 0);
                break;
            case 'fast':
                $(clickedItem).attr('slow', 0);
                break;
            case 'first':
                $(clickedItem).attr('last', 0);
                break;
            case 'last':
                $(clickedItem).attr('first', 0);
                break;
        }
        $(clickedItem).attr(status, 1);
    }

    savedata();
}
var menu = [{
    name: 'Configure',
    title: 'Configure',
    subMenu: [{
        'name': 'reverse',
        'className': 'reverse',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'reverse');
        }
    }, {
        'name': 'flip',
        'className': 'flip',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'flip');
        }
    }]
}, {
    name: 'remove',
    title: 'remove',
    fun: removeAll,
}];


var menuTag = [{
    name: 'Configure',
    title: 'Configure',
    subMenu: [{
        'name': 'reverse',
        'className': 'reverse',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'reverse');
        }
    }, {
        'name': 'flip',
        'className': 'flip',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'flip');
        }
    }, {
        name: 'Order',
        title: 'Order',
        subMenu: [{
            'name': 'first',
            'className': 'first',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'first');
            }
        }, {
            'name': 'last',
            'className': 'last',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'last');
            }
        }]
    }, {
        name: 'Speed',
        title: 'Speed',
        subMenu: [{
            'name': 'Slow',
            'className': 'slow',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'slow');
            }
        }, {
            'name': 'Fast',
            'className': 'fast',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'fast');
            }
        }]
    }, {
        'name': 'Envelope',
        'className': 'envelope',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'envelope');
        }
    }, {
        'name': 'Effect',
        'className': 'effect',
        'img': 'img/uncheck.png',
        fun: function (item) {
            changeStatus(item, 'effect');
        }
    }, {
        name: 'Tag Location in clip',
        title: 'Tag Location in clip',
        subMenu: [{
            'name': 'beginning',
            'className': 'beginning',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'beginning');
            }
        }, {
            'name': 'end',
            'className': 'end',
            'img': 'img/uncheck.png',
            fun: function (item) {
                changeStatus(item, 'end');
            }
        }]
    }]

}, {
    name: 'remove',
    title: 'remove',
    fun: removeOne,

}];

function contextOpen(item) {
    var clickedItem = item.trigger[0];
    var clickedMenu = item.menu[0];
    console.log(clickedMenu);

    var i = 0;
    for (i = 0; i < configs.length; i++) {
        if ($(clickedItem).attr(configs[i]) == '1') {
            $(clickedMenu).find('.' + configs[i] + ' img').attr('src', 'img/check.png');
        } else {
            $(clickedMenu).find('.' + configs[i] + ' img').attr('src', 'img/uncheck.png');
        }
    }
}


$(document).ready(function () {
    var liList = $("#playlist li");

    if (liList.length > 0) {

        $(liList[0]).css("border-color", "rgb(103, 247, 233)");
        $(liList[0]).css("border-width", "3px");
        $(liList).css("overflow-wrap", "break-word");
        $(liList).css("white-space", "normal");
    }

    $("li[id*='_file']").contextMenu(menu, { triggerOn: 'click', mouseClick: 'right', onOpen: contextOpen });

    //$("li[id*='_tag']").contextMenu(menuTag, { triggerOn: 'click', mouseClick: 'right' });

    //$('ul').on('contextmenu', 'li', function (e) {
    //    e.preventDefault();
    //    $('#' + this.id).remove();
    //});

    //Calling context menu
    // $('#1_file').contextMenu(menu, { triggerOn: 'click', mouseClick: 'right' });

    function loadFile(file) {

        var URL = window.URL || window.webkitURL
        var elem = document.getElementById("id_upload");

        //var file = elem.files[0];                
        //var fileURL = URL.createObjectURL(file)
        //video.src = fileURL;

        var video = $(file).attr("movieurl");
        var fileURL = video;
        $("#videoarea").attr({
            "src": fileURL,
            "poster": "",
            // "autoplay": "autoplay"
        })

        var liList = $("#playlist li");
        var tagList = $("#tagList li");

        for (var i = 0; i < liList.length; i++) {
            $(liList[i]).css("border-color", "#ddd");
            $(liList[i]).css("border-width", "1px");

            $(tagList[i]).css("border-color", "#ddd");
            $(tagList[i]).css("border-width", "1px");
        }

        var id = $(file).attr('id').split('_')[0];
        var tag = $('#' + id + '_tag');
        if ($(file).css("border-color") === "rgb(221, 221, 221)") {
            $(file).css("border-color", "rgb(103, 247, 233)");
            $(file).css("border-width", "3px");
            $(file).css("border-right-width", "1px");
            $(file).css("border-right-color", "rgb(221, 221, 221)");
        }
        else {
            $(file).css("border-color", "rgb(221, 221, 221)");
            $(file).css("border-right-width", "1px");
            $(file).css("border-right-color", "rgb(221, 221, 221)");
        }

        if (tag.css("border-color") === "rgb(221, 221, 221)") {
            tag.css("border-color", "rgb(103, 247, 233)");
            tag.css("border-width", "3px");
            tag.css("border-left-width", "1px");
            tag.css("border-left-color", "rgb(221, 221, 221)");
        }
        else {
            tag.css("border-color", "rgb(221, 221, 221)");
            tag.css("border-left-width", "1px");
            tag.css("border-left-color", "rgb(221, 221, 221)");
        }

        $('#tagList a').css('border-color', '#ccc');
        $('#tagList a').css('border-width', '1px');

        currentShot = null;

    }

    $("#tagList li").click(function (e) {
        if (e.target !== this)
            return;
        var mainDivOffsetTop = document.getElementById("main").offsetTop;
        var listOffsetTop = this.offsetTop;
        document.getElementById("main").scrollTop = listOffsetTop - mainDivOffsetTop;

        var seqNumber = $(this).attr('id').split('_')[0];
        var fileId = seqNumber.toString() + "_file";

        var file = $('#' + fileId);
        loadFile(file);

    });

    $("#playlist li").click(function (e) {

        var mainDivOffsetTop = document.getElementById("main").offsetTop;
        var listOffsetTop = this.offsetTop;
        document.getElementById("main").scrollTop = listOffsetTop - mainDivOffsetTop;

        loadFile(this);
              
    });

    $.ajax({
        type: "POST",
        url: "Dashboard.aspx/SetInitialCount",
        data: "{'user':'" + document.getElementById('hdnuser').value + "','project':'" + document.getElementById('hdnproject').value + "','decimator':'" + document.getElementById('txtdecimator').value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log('Getting initial counts for tags');
            var minMax = response.d.split(',');

            document.getElementById('minCount').textContent = minMax[0];
            document.getElementById('maxCount').textContent = minMax[1];
            document.getElementById('needCount').textContent = minMax[0] - currentcount;
            document.getElementById('avaiCount').textContent = minMax[1] - currentcount;

        },
        failure: function (response) {
            alert(response.d);
        }
    });


    $('.repeater').repeater({
        defaultValues: {
            'textarea-input': 'foo',
            'text-input': 'bar',
            'select-input': 'B',
            'checkbox-input': ['A', 'B'],
            'radio-input': 'B'
        },
        show: function () {
            $(this).slideDown();
        },
        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },
        ready: function (setIndexes) {

        }
    });

    window.outerRepeater = $('.outer-repeater').repeater({
        isFirstItemUndeletable: true,
        defaultValues: { 'text-input': 'outer-default' },
        show: function () {
            console.log('outer show');
            $(this).slideDown();
        },
        hide: function (deleteElement) {
            console.log('outer delete');
            $(this).slideUp(deleteElement);
        },
        repeaters: [{
            isFirstItemUndeletable: true,
            selector: '.inner-repeater',
            defaultValues: { 'inner-text-input': 'inner-default' },
            show: function () {
                console.log('inner show');
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                console.log('inner delete');
                $(this).slideUp(deleteElement);
            }
        }]
    });

    $("#playlist li").on("click", function () {
        //$("li").removeClass("active");
        //$(this).addClass("active");

        var tag = document.getElementById("button");
        tag.value = $(this)[0].id.split("_")[0];

        // $('#' + tag.value + '_tag').prependTo($('#' + tag.value + '_tag').parent());
        // $('#' + tag.value + '_file').prependTo($('#' + tag.value + '_file').parent());
    });

    $("#playlist li:first").trigger('click');

    $('#videoarea')[0].ontimeupdate = function (event) {
        var time = $('#videoarea')[0].currentTime;

        var id = $('#button').val();
        if (id) {
            currentShot = null;
            var i = 0;
            for (i = 0; i < $('#' + id + '_tag').children().length; i++) {
                if (Math.floor($($('#' + id + '_tag').children()[i]).attr('time')) <= Math.floor(time)) {
                    currentShot = $('#' + id + '_tag').children()[i];
                } else {
                    break;
                }
            }

            $('#tagList a').css('border-color', '#ccc');
            $('#tagList a').css('border-width', '1px');
            if (currentShot != null) {

                $(currentShot).css('border-color', 'red');
                $(currentShot).css('border-width', '2px');
                changeShotScroll();
            }
        }
    };
    //var list, i, switching, b, shouldSwitch;
    //list = document.getElementById("playlist");
    //switching = true;
    ///*Make a loop that will continue until
    //no switching has been done:*/
    //while (switching) {
    //    //start by saying: no switching is done:
    //    switching = false;
    //    b = list.getElementsByTagName("LI");
    //    //Loop through all list-items:
    //    for (i = 0; i < (b.length - 1) ; i++) {
    //        //start by saying there should be no switching:
    //        shouldSwitch = false;
    //        /*check if the next item should
    //        switch place with the current item:*/
    //        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
    //            /*if next item is alphabetically
    //            lower than current item, mark as a switch
    //            and break the loop:*/
    //            shouldSwitch = true;
    //            break;
    //        }
    //    }
    //    if (shouldSwitch) {
    //        /*If a switch has been marked, make the switch
    //        and mark the switch as done:*/
    //        b[i].parentNode.insertBefore(b[i + 1], b[i]);
    //        switching = true;
    //    }
    //}



});
