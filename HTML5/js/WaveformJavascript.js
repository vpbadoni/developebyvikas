﻿var mylist;
var wavesurfer;
var regionArr = new Array();
var regionIDArr = new Array();
var countreg = 1;
var now = 0.00;
function getSonglist(songurl) {

    var data = songurl.split(',');
    var songdiv = document.getElementById('DVMP3File');
    var resetmark = "resetmark";
   

    $('#playlistbox').append('<a id="playTrack" href="#" class="col-md-4" style="height:128px;">' + data[0] + '</a><br/> ');

    $('#btnDiv').append('<a class="btn btn-primary" href="#" data-toggle="tooltip" title="Reset all Marker!"   onclick="DrawRegionAfterLoad( \'' + resetmark + '\');" style="padding: 4px 9px;"> <i class="fa fa-refresh"></i></a><a class="btn btn-primary" data-toggle="tooltip" title="Save all Marker!" href="#" style="padding: 4px 9px;" > <i class="fa fa-floppy-o"></i></a><a class="btn btn-primary" data-toggle="tooltip" title="Delete Odd Marker!" onclick="DeleteOddRegions();" href="#" style="padding: 4px 9px;"> <i class="glyphicon glyphicon-queen"></i></a>');
    wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: '#D2EDD4',
        progressColor: 'Yellow',
        cursorColor: '#fff',
        markerWidth: 2
    });

    wavesurfer.load(data[1]);



    wavesurfer.on('ready', function () {
        var timeline = Object.create(WaveSurfer.Timeline);

        timeline.init({
            wavesurfer: wavesurfer,
            container: '#waveform-timeline'

        });

        var Onloading = "loaddata";

        DrawRegionAfterLoad(Onloading);
    });

    wavesurfer.on('region-update-end', function (region, event) {
        console.log("entered method: region-update-end");

        var start_time = region.start;
        var RegID = region.id;
        // $("#SelectionTime").text(start_time);

        var indexR = regionIDArr.indexOf(RegID);
        var start_time = region.start;


        regionArr[indexR] = start_time;
        SortTheArray(regionArr, regionIDArr);

        if (!region.hasDeleteButton) {
            var regionEl = region.element;

            var deleteButton = regionEl.appendChild(document.createElement('deleteButton'));
            deleteButton.className = 'fa fa-trash';



            deleteButton.addEventListener('click', function (e) {
                var Xstarttime = region.start;
                removeArrayElement(Xstarttime);
                region.remove();
            });

            deleteButton.title = "Delete Mark";

            var css = {
                display: 'block',
                float: 'right',
                padding: '3px',
                position: 'relative',
                zIndex: 10,
                cursor: 'pointer',
                cursor: 'hand',
                color: '#FFFFFF'
            };

            region.style(deleteButton, css);
            region.hasDeleteButton = true;
        }
    });

    $('#waveform').on('mousewheel', function (e) {
        if (e.originalEvent.wheelDelta > 0) {
            slider = slider + 10;
            if (slider > sliderMax) {
                slider = sliderMax;
            }
            var zoomLevel = Number(slider);
            wavesurfer.zoom(zoomLevel);

            ResizeRegionOnZooming(slider);
        }
        else {
            slider = slider - 10;
            if (slider < 0) {
                slider = 0;
            }
            var zoomLevel = Number(slider);
            wavesurfer.zoom(zoomLevel);
            // ResizedecreaseNewResion(slider);
            ResizeRegionOnZooming(slider);
        }
    });

    wavesurfer.on('region-mouseup', function (region, event) {

        SortTheArray(regionArr, regionIDArr);
    });

}

function wavesurferplayPause() {
    wavesurfer.play();
}

function DrawRegionAfterLoad(onloadcheck) {
    if (onloadcheck == "resetmark") {
    event.preventDefault();
    }
    regionArr.length = 0;
    regionIDArr.length = 0;
    //wavesurfer.enableDragSelection({});
    var duration = wavesurfer.getDuration();
    var timeframe = duration / 200;
    var clipStarttime = timeframe;
    var clipEndtime = 0;
    var demotext = "";
    for (var i = 1; i <= 200; i++) {
        clipEndtime = clipStarttime + timeframe;
        wavesurfer.addRegion({
            id: 'Sample' + countreg,
            start: clipStarttime, // time in seconds
            end: clipStarttime + 0.0925, // time in seconds
            color: 'Yellow',
            resize: false
        });
        regionArr.push(clipStarttime);
        clipStarttime = clipEndtime;
        regionIDArr.push('Sample' + countreg);
        countreg = countreg + 1;
        // demotext = demotext + "</br>" + "StartTime = " + clipStarttime + "  Endtime = " + clipStarttime + 0.08;
    }
    SortTheArray(regionArr, regionIDArr);
}

var slider = 0;
var sliderMax = 300;



function ResizeRegionOnZooming(Slidervalue) {

    // regionArr.length = 0;
    regionIDArr.length = 0;
    countreg = 1;
    var newadd = 0;
    wavesurfer.clearRegions();
    // wavesurfer.enableDragSelection({});

    newadd = GetNewAddvalue();
    for (var i = 0; i < regionArr.length; i++) {
        //clipEndtime = regionArr[i];
        wavesurfer.addRegion({
            id: 'Sample' + countreg,
            start: regionArr[i], // time in seconds
            end: regionArr[i] + newadd, // time in seconds
            color: 'Yellow',
            resize: false
        });
        // this.wavesurfer.fireEvent('region-update-end', this);
        regionIDArr.push('Sample' + countreg);
        countreg = countreg + 1;
    }

    SortTheArray(regionArr, regionIDArr);

}


//wavesurfer.on('ready', function () {
//    var timeline = Object.create(WaveSurfer.Timeline);

//    timeline.init({
//        wavesurfer: wavesurfer,
//        container: '#waveform-timeline'

//    });
//});

$('body').on('click', '.MarkMP', function () {

    now = wavesurfer.getCurrentTime();

    newadd = GetNewAddvalue();

    wavesurfer.addRegion({
        id: 'Sample' + countreg,
        start: now, // time in seconds
        end: now + newadd, // time in seconds
        color: 'Red',
        resize: false
    });

    regionIDArr.push('Sample' + countreg);
    countreg = countreg + 1;

    regionArr.push(now);

    SortTheArray(regionArr, regionIDArr);


    //console.log(now);
});


$('body').on('click', '.DeleteMarkMP', function () {
    now = "";
    regionArr.length = 0;
    regionIDArr.length = 0;
    wavesurfer.clearRegions();
    //document.getElementById('SelectionTime').text = now;
    $("#SelectionTime").text(" ");

});



function DeleteOddRegions() {
    event.preventDefault();
    for (var i = 0; i < regionArr.length; i++) {
        regionArr.splice(i + 1, 1);
    }
    regionIDArr.length = 0;
    countreg = 1;
    var newadd = 0;
    wavesurfer.clearRegions();
    // wavesurfer.enableDragSelection({});

    newadd = GetNewAddvalue();

    //var i = wavesurfer.region.length;

   


    for (var i = 0; i < regionArr.length; i++) {
        //clipEndtime = regionArr[i];
        wavesurfer.addRegion({
            id: 'Sample' + countreg,
            start: regionArr[i], // time in seconds
            end: regionArr[i] + newadd, // time in seconds
            color: 'red',
            resize: false
        });
        // this.wavesurfer.fireEvent('region-update-end', this);
        regionIDArr.push('Sample' + countreg);
        countreg = countreg + 1;
    }

    SortTheArray(regionArr, regionIDArr);
}




function GetNewAddvalue() {
    if (slider != 0)
        newadd = 0.05 - (0.05 * slider / 200)    //200 is the Double percentage value
    else
        newadd = 0.0925;

    if (slider == 10) {
        newadd = 0.0925;
    }

    return newadd;
}




function removeArrayElement(RStartTime) {
    var i = regionArr.indexOf(RStartTime);
    if (i != -1) {
        regionArr.splice(i, 1);
    }

    var regiontext = "";
    for (var i = 0; i < regionArr.length; i++) {
        regiontext = regiontext + regionArr[i].toFixed(2) + " ";
    }

    PassArraytoCode();

}










function SortTheArray(RegionTime, RegionID) {
    var length = RegionTime.length;
    for (var i = 0; i < length - 1; i++) {
        //Number of passes
        var min = i; //min holds the current minimum number position for each pass; i holds the Initial min number
        for (var j = i + 1; j < length; j++) { //Note that j = i + 1 as we only need to go through unsorted array
            if (RegionTime[j] < RegionTime[min]) { //Compare the numbers
                min = j; //Change the current min number position if a smaller num is found
            }
        }
        if (min != i) {
            //After each pass, if the current min num != initial min num, exchange the position.
            //Swap the numbers 
            var tmp = RegionTime[i];
            RegionTime[i] = RegionTime[min];
            RegionID[i] = RegionID[min];
            RegionTime[min] = tmp;
        }
    }

    PassArraytoCode();
}


